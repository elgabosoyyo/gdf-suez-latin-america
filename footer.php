		<div class="w1000">
			<footer class="w1000">
				<div class="foot-section" id="fs1">
					<span>Our Values:</span><br />
					Drive<br />
					Commitment<br />
					Daring<br />
					Cohesion
				</div>
<?php
	$args = array(
		'sort_order' => 'ASC',
		'sort_column' => 'menu_order',
		'child_of' => 4146,
		'parent' => 4146,
		'post_type' => 'page',
		'posts_per_page'=>	-1,
		'post_status' => 'publish'
		);
		
	$pages=get_pages($args);
	foreach($pages as $i=>$v)
		{if($i%3==0)
			{
?>
				<div class="foot-section">
<?php
			}
?>
					<div>
						<a href="<?php echo link_to_follow($v, false); ?>"><?php _e($v->post_title); ?></a>
					</div>
<?php
		}
?>
				</div>
				<!--
				<div id="rounds">
					<a href="http://www.gdfsuez.com/en/subscribe-to-the-rss-feed/" id="feed"></a>
					<a href="http://www.youtube.com/gdfsuez" id="youtube-round"></a>
					<a href="http://www.facebook.com/gdfsuez" id="facebook-round"></a>
					<a href="http://www.twitter.com/gdfsuez" id="twitter-round"></a>
				</div>
				-->
			</footer>
			<div id="colors">
				<div class="colors" id="violet"></div>
				<div class="colors" id="turquoise"></div>
				<div class="colors" id="orange"></div>
				<div class="colors" id="white"></div>
				<div class="colors" id="blue"></div>
				<div class="colors" id="light-blue"></div>
				<div class="colors" id="yellow"></div>
				<div class="colors" id="green"></div>
				<div class="colors" id="light-green"></div>
				<div class="colors" id="red"></div>
			</div>
		</div>
	</body>
</html>