<?php
	/* template name: Search */
	get_header();
?>
		<div id="search" class="w1000">
<?php
	switch (qtrans_getLanguage())
		{case "en":
			echo "Search";
			break;
		case "pt":
			echo "Pesquisa";
			break;
		case "es":
			echo "Buscar";
			break;
		}
	
	if(isset($_GET["q"]))
		{$_SESSION["search"]=$_GET["q"];
		if($_GET["q"]!="")
			{$_SESSION["results"]=array();
			$args=array(
				'posts_per_page'=>-1,
				'orderby'=> 'post_date',
				'order'=> 'DESC',
				'post_type'=> 'post',
				'post_status'=>'publish'
				);
			
			$posts=get_posts($args);
			foreach($posts as $i=>$v)
				if(strpos(strtolower($v->post_title), strtolower($_GET["q"]))!==false || strpos(strtolower($v->post_content), strtolower($_GET["q"]))!==false)
					$_SESSION["results"][]=$v;
			
			$explode=explode(" ", $_GET["q"]);
			foreach($explode as $word)
				foreach($posts as $i=>$v)
					if(strpos(strtolower($v->post_title), strtolower($word))!==false || strpos(strtolower($v->post_content), strtolower($word))!==false)
						if(!in_array($v, $_SESSION["results"]));
							$_SESSION["results"][]=$v;
			}
		}
	?>
		</div>
		<section id="search-section" class="w1000">
			<div id="form-search">
				<form action="" method="get" id="search-form">
					<input type="hidden" name="page" value="1" />
					<input type="text" name="q" value="<?php echo isset($_GET["q"]) ? $_GET["q"] : ""; ?>" /> <input type="submit" id="submit" value="" />
				</form>
				<span>
<?php
	if(isset($_SESSION["results"]))
		{switch (qtrans_getLanguage())
			{case "en":
				echo count($_SESSION["results"])<10 ? count($_SESSION["results"]):"10";
				echo " on " . count($_SESSION["results"]) . " results for";
				break;
			case "pt":
				echo count($_SESSION["results"])<10 ? count($_SESSION["results"]):"10";
				echo " de " . count($_SESSION["results"]) . " resultados para";
				break;
			case "es":
				echo count($_SESSION["results"])<10 ? count($_SESSION["results"]):"10";
				echo " de " . count($_SESSION["results"]) . " resultados para";
				break;
			}
		?></span>
<?php
		echo '"' . $_SESSION["search"] .'"';
		if(count($_SESSION["results"])!=0)
			{
	?>
			</div>
			<div id="results">
<?php
			$c=$GLOBALS["page"]*10;
			$cont=$c-10;
			do
				{if(!isset($_SESSION["results"][$cont]))
					break;
?>
				<a href="<?php echo site_url(); ?>/<?php echo qtrans_getLanguage(); ?>/<?php echo $_SESSION["results"][$cont]->post_name; ?>" class="results">
					<div class="tn">
						<div class="result-number"><?php echo $cont+1<10 ? "0":""; echo $cont+1; ?></div>
						<div class="result-title">
							<?php _e($_SESSION["results"][$cont]->post_title); ?>
						</div>
					</div>
					<div class="content">
						<?php echo get_content($_SESSION["results"][$cont], "", 126); ?>
					</div>
				</a>
<?php
				$cont++;
				}while($cont<$c);
			$pages=ceil(count($_SESSION["results"])/10);
?>
			</div>
			<div id="pagination">
				<a href="<?php echo site_url(); ?>/<?php echo qtrans_getLanguage(); ?>/seeker/<?php echo $GLOBALS["page"]-1; ?>/" style="<?php echo $GLOBALS["page"]>1 ? "display: block;":""; ?>" class="prev"></a>
				<a href="<?php echo site_url(); ?>/<?php echo qtrans_getLanguage(); ?>/seeker/1/" class="<?php echo $GLOBALS["page"]==1 ? "active":""; ?>">1</a>
				<div style="<?php echo $GLOBALS["page"]>6 ? "display: block;":""; ?>" class="ppp">...</div>
<?php
			$cont=0;
			for($c=$GLOBALS["page"]-6>0 ? $GLOBALS["page"]-6+2:2;$c<$pages;$c++)
				{if($cont==9)
					break;
?>
				<a href="<?php echo site_url(); ?>/<?php echo qtrans_getLanguage(); ?>/seeker/<?php echo $c; ?>/" class="<?php echo $GLOBALS["page"]==$c ? "active":""; ?>"><?php echo $c; ?></a>
<?php
				$cont++;
				}
			
			if($pages==1)
				$c=1;
?>
				<div style="<?php echo $c<$pages && $pages>9 ? "display: block;":""; ?>" class="ppp">...</div>
				<a href="<?php echo site_url(); ?>/<?php echo qtrans_getLanguage(); ?>/seeker/<?php echo $pages; ?>/" style="<?php echo $pages>1 ? "display: block;":"display: none;"; ?>" class="<?php echo $GLOBALS["page"]==$pages ? "active":""; ?>"><?php echo $pages; ?></a>
				<a href="<?php echo site_url(); ?>/<?php echo qtrans_getLanguage(); ?>/seeker/<?php echo $pages; ?>/" style="<?php echo $GLOBALS["page"]>$pages ? "display: block;":""; ?>" class="next"></a>
			</div>
<?php
			}
		}
	?>
		</section>
		<div id="line-down"></div>
		<script>
			
		</script>
<?php get_footer(); ?>