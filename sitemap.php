<?php
	/* template name: Sitemap */
	get_header();
?>
		<section id="sitemap">
			<div id="pages">
				<h2>Pages</h2>
<?php
	construct_menu(3272, "ALL", "", "first-ul");
?>
			</div>
			
			<div id="posts">
				<h2>Posts</h2>
				<ul class="ul-menu first-ul">
<?php
	// Add categories you'd like to exclude in the exclude here
	$cats = get_categories('exclude=');
	foreach($cats as $cat)
		{
?>
					<li id="<?php echo $cat->term_id; ?>"><?php echo $cat->cat_name; ?> <span class="plus-minus plus" data-id="<?php echo $cat->term_id; ?>" onclick="plusMinus(this)"></span>
						<ul class="ul-menu">
<?php
		query_posts('posts_per_page=-1&cat='.$cat->cat_ID);
		while(have_posts())
			{the_post();
			$category = get_the_category();
			// Only display a post link once, even if it's in multiple categories
			if($category[0]->cat_ID == $cat->cat_ID)
				{
?>
							<li><a href="<?php echo get_permalink(); ?>"><?php echo get_title(get_the_id(), 30); ?></a></li>
<?php
				}
			}
?>
						</ul>
<?php	} ?>
					</li>
				</ul>
			</div>
		</section>
<?php get_footer(); ?>