<?php
	session_start();
	
	if(@$post->post_parent==3272 and "b.php"==get_post_meta(@$post->ID, '_wp_page_template', true))
		{$args = array(
			'sort_order' => 'ASC',
			'sort_column' => 'menu_order',
			'child_of' => $post->ID,
			'parent' => $post->ID,
			'post_type' => 'page',
			'posts_per_page'=>	1,
			'post_status' => 'publish'
			);
		
		$page=get_pages($args);
		header('Location: ' . get_permalink($page[0]->ID));
		}
	
	$fields=@get_field_objects($post->ID);
	if(isset($fields["redirect_to"]["value"]))
		if($fields["redirect_to"]["value"]!="")
			header('Location: ' . $fields["redirect_to"]["value"]);
	
	$args = array(
		'sort_order' => 'ASC',
		'sort_column' => 'menu_order',
		'child_of' => 3272,
		'parent' => 3272,
		'post_type' => 'page',
		'post_status' => 'publish'
		);
	
	$pages = get_pages($args);
?>
<!DOCTYPE html>
<html lang="<?php echo qtrans_getLanguage(); ?>">
	<head>
		<title>GDF Suez <?php echo __("[:en]Latin America[:es]Latinoamérica[:pt]América Latina") . __(wp_title('-', false)); ?></title>
		<meta name="keywords" lang="ES" content="Las necesidades de energía, la seguridad del suministro, el cambio climático, el uso de recursos" />
		<meta name="keywords" lang="EN" content="Energy needs, security of supply, climate change, resource use" />
		<meta name="keywords" lang="PT" content="As necessidades de energia, segurança do aprovisionamento, alterações climáticas, o uso de recursos" />
		<meta name="description" lang="ES" content="GDF SUEZ Energy Latin America provee soluciones energéticas innovadoras y gas en Argentina, Brasil, Chile, Costa Rica, Panamá y Perú, el apoyo a este continente emergente en su crecimiento económico, respetando el medio ambiente y proporcionando servicios esenciales a su población." />
		<meta name="description" lang="EN" content="GDF SUEZ Energy Latin America provides innovative energy and gas solutions in Argentina, Brazil, Chile, Costa Rica, Panama and Peru, supporting this emerging continent in its economic growth, respecting the environment and providing essential services to its people." />
		<meta name="description" lang="PT" content="GDF SUEZ Energy Latin America oferece soluções inovadoras para energia e gás na Argentina, Brasil, Chile, Costa Rica, Panamá e Peru, apoiando este continente emergente no seu crescimento econômico, respeitando o meio ambiente e prestação de serviços essenciais ao seu povo." />
		<meta name="robots" content="INDEX, FOLLOW" />
		<meta name="author" content="Gabriel Scocozza, The Amalgama" />
		<meta charset="utf-8" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<link type="image/x-icon" rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" />
		
		<!--<script>
			/*window.fbAsyncInit = function()
				{FB.init(
					{appId: '168946969485',
					status: true,
					xfbml: true
					});
				};*/
		</script>-->
		<!--[if IE]>
			<script src="<?php bloginfo("template_url"); ?>/js/html5shiv-printshiv.js"></script>
		<![endif]-->
		<script src="<?php bloginfo("template_url"); ?>/js/jquery-1.11.0.min.js"></script>
		<script src="<?php bloginfo("template_url"); ?>/js/jquery.nicescroll.min.js"></script>
		<script src="<?php bloginfo("template_url"); ?>/js/jquery.cycle.all.js"></script>
		<script src="<?php bloginfo("template_url"); ?>/js/skrollr.min.js"></script>
		
		<script>
			var dir="<?php bloginfo("template_url"); ?>";
			var title="GDF Suez <?php echo __("[:en]Latin America[:es]Latinoamérica[:pt]América Latina") . " - " . __(wp_title('', false)); ?>";
			var titlet="<?php _e(wp_title('', false)); ?>";
			var urls="<?php echo site_url(); ?>";
		</script>
		<!--[if IE]>
			<script src="<?php bloginfo("template_url"); ?>/js/skrollr.ie.min.js"></script>
		<![endif]-->
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />
		<script src="<?php bloginfo("template_url"); ?>/js/functions.js"></script>
	</head>
	<body>
		<!--<div id="fb-root"></div>
		<script id='facebook-jssdk' src="<?php bloginfo("template_url"); ?>/js/facebook.js"></script>-->
		
		<div id="flotante"></div>
		<div id="warning"><?php _e("[:es]Aviso: para poder ver esta página correctamente en Internet Explorer, necesita al menos la versión 8, usted esta usando una versión inferior[:en]Warning: to view this page correctly in Internet Explorer, you need at least version 8, you are using a lower version[:pt]Nota: para visualizar esta página corretamente no Internet Explorer, você precisa de pelo menos a versão 8, você está usando uma versão inferior"); ?></div>
		<div id="menu-a-float" class="menu-float">
<?php
	construct_menu(3272, array("a.php","a-child.php"), "", "first-ul");
?>
		</div>
		<div id="menu-b-float" class="menu-float">
<?php
	construct_menu(3272, array("b.php"),"first-ul", "");
?>
		</div>
		<div id="colors">
			<div class="colors" id="violet"></div>
			<div class="colors" id="turquoise"></div>
			<div class="colors" id="orange"></div>
			<div class="colors" id="white"></div>
			<div class="colors" id="blue"></div>
			<div class="colors" id="light-blue"></div>
			<div class="colors" id="yellow"></div>
			<div class="colors" id="green"></div>
			<div class="colors" id="light-green"></div>
			<div class="colors" id="red"></div>
		</div>
		<header>
			<div class="w1000">
				<div class="hamburger" data="menu-b-float"></div>
				<a id="logo-link" href="<?php echo site_url(); ?>/"><img src="<?php bloginfo("template_url"); ?>/images/logo.png" id="logo" /></a>
				<div class="menu-right" data="menu-a-float"></div>
				<div id="lang-head"></div>
				<form action="<?php echo site_url(); ?>/seeker" method="get" id="form-search-header">
					<input type="text" name="q" placeholder="<?php _e("[:en]Search[:es]Búsqueda[:pt]Pesquisa"); ?>" /><input type="button" id="magglass-930" />
				</form>
				<div id="menu">
					<!--<div id="up">
						<span class="name">GSZ</span><div id="monetario1">19,040€ <span class="up-down"></span> <span class="dif">(+0,85 %)</span></div><span class="name">CAC 40</span><div id="monetario2">4 417,04 <span class="up-down"></span> <span class="dif">(+0,59 %)</span></div><div id="socials"><a id="globos" target="_blank" href="javascript:void(0);" class="social"></a> <a id="youtube" target="_blank" href="http://youtube.com/gdfsuez" class="social"></a> <a id="facebook" target="_blank" href="http://facebook.com/gdfsuez" class="social"></a> <a id="twitter" target="_blank" href="http://twitter.com/gdfsuez" class="social"></a></div>
					</div>-->
					<div id="down">
<?php
	foreach($pages as $v)
		{if(get_post_meta($v->ID, '_wp_page_template', true)=="a.php")
			{
?>
					<div><a href="<?php echo get_permalink($v); ?>"><?php _e($v->post_title); ?></a></div>
<?php
			}
		}
?>
					</div>
				</div>
			</div>
		</header>
		<div id="sub-menu">
			<div class="w1000">
				<ul>
<?php
	foreach($pages as $v)
		{if(get_post_meta($v->ID, '_wp_page_template', true)=="b.php")
			{
?>
					<li class="subleft" data-deploy="<?php _e($v->post_title); ?>"><a href="<?php echo get_permalink($v); ?>"><?php _e($v->post_title); ?></a></li>
<?php
			}
		}
?>
				</ul>
				<a href="<?php echo site_url(); ?>/seeker" id="magglass" class="subright"></a>
				<div id="lang" class="subright"><?php echo qtrans_getLanguage(); ?> <div class="arrow-down"></div></div>
				<div id="languages">
					<a href="<?php echo qtrans_convertURL(get_permalink(), 'en'); ?>" <?php echo qtrans_getLanguage()=="en" ? "style='display: none'" : ""; ?>>EN</a>
					<a href="<?php echo qtrans_convertURL(get_permalink(), 'es'); ?>" <?php echo qtrans_getLanguage()=="es" ? "style='display: none'" : ""; ?>>ES</a>
					<a href="<?php echo qtrans_convertURL(get_permalink(), 'pt'); ?>" <?php echo qtrans_getLanguage()=="pt" ? "style='display: none'" : ""; ?>>PT</a>
				</div>
			</div>
		</div>
<?php
	$args = array(
		'sort_order' => 'ASC',
		'sort_column' => 'menu_order',
		'child_of' => 3272,
		'parent' => 3272,
		'post_type' => 'page',
		'post_status' => 'publish');
	
	$posts=get_pages($args);
	
	foreach($posts as $v)
		{if(get_post_meta($v->ID, "_wp_page_template", true)!="b.php")
			continue;
?>
		<div class="menu w1000" data-id="<?php _e($v->post_title); ?>">
<?php build_b($v); ?>
		</div>
<?php	} ?>