	<div class="new">
		<a href="<?php echo site_url(); ?>/press">
			<div class="new-title">
				<?php _e("[:en]PRESS<br />RELEASE[:es]comunicado<br />de prensa[:pt]comunicado<br />de imprensa"); ?>
			</div>
		</a>
<?php
	$npc=get_post_wp(array('press',"Press Releases"), 2);
	foreach($npc as $v)
		{$date=get_date($v);
?>
		<a href="<?php echo qtrans_convertURL(get_permalink($v->ID), qtrans_getLanguage()) ?>">
			<div>
				<div class="new-body">
					<div class="new-info">
						<?php echo get_title($v, 58); ?>
					</div>
					<div class="new-date">
						<div class="new-day">
							<?php echo @$date[2]; ?>
						</div>
						<div class="new-month">
							<?php echo @$date[1]; ?>
						</div>
						<div class="new-year">
							<?php echo @$date[0]; ?>
						</div>
					</div>
				</div>
			</div>
		</a>
<?php	} ?>
	</div>