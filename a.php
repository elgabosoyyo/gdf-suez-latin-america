<?php
	/* template name: Menu A */
	get_header();
	
	$args = array(
		'sort_order' => 'ASC',
		'sort_column' => 'post_date',
		'child_of' => $post->ID,
		'parent' => $post->ID,
		'post_type' => 'page',
		'post_status' => 'publish');
	
	$pages=get_pages($args);
	
?>
		<section class="main-section w1000">
			<div class="section-title">
				<?php _e($post->post_title); ?>
			</div>
			<div class="section-left">
				<?php include 'get_press.php'; ?>
<?php
	foreach($pages as $v)
		{if(strpos(strtolower(__($v->post_title)), "box")===false)
			continue;
		
		$fields=get_field_objects($v->ID);
		$pages=explode(",", __($v->post_content));
?>
				<div class="direct-access" style="background-color: <?php echo $fields["background_color"]["value"]; ?>; color: <?php echo $fields["text_color"]["value"]; ?>">
					<h3><?php echo substr(__($v->post_title),4); ?></h3>
					<ul>
<?php
		foreach($pages as $i)
			{$page=get_post($i);
?>
						<li><a href="<?php echo get_permalink($page->ID); ?>" style="color: <?php echo $fields["text_color"]["value"]; ?>"><?php echo get_title($page, 25); ?></a></li>
<?php		} ?>
					</ul>
				</div>
<?php	} ?>
			</div>
			<div class="section-right">
				<div id="cicle">
<?php
	$galeries=get_post_galleries($post,false);
	$galeries=explode(",", $galeries[0]["ids"]);
	foreach($galeries as $v)
		{$img_data=get_post($v);
		$attach=wp_get_attachment_image_src($v, 'slider-a');
		$url=get_post_meta($v, '_gallery_link_url', true);
		$target=get_post_meta($v, '_gallery_link_target', true);
		if($target=="")
			$target="_self";
?>
					<a href="<?php echo $url; ?>" target="<?php echo $target; ?>">
						<div class="slider">
							<div class="new-black">
								<div class="slider-title">
									<?php _e($img_data->post_title); ?>
								</div>
								<div class="slider-desc">
									<?php _e($img_data->post_excerpt); ?>
								</div>
							</div>
							<img src="<?php echo $attach[0]; ?>" />
						</div>
					</a>
<?php	} ?>
				</div>
				<div id="pager"></div>
			</div>
<?php
	$pages=get_posts(array(
	'post_type'=> array("page","attachment"),
	'post_status' => array("publish", "inherit"),
	'meta_query' => array(
		array(
			'key' => 'size_' . ucfirst($post->post_name),
			'value' => Array('Column Banner', 'Small rectangle box', 'Small Box', 'Small Box With Text'),
			'compare' => 'IN'
			)
		)));
	
	$small=array();
	$small_rectangle=array();
	$exit=false;
	while((count($small_rectangle)<2 or count($small)<3 or !isset($column)) and !$exit)
		{foreach($pages as $v)
			{$pm=get_post_meta($v->ID, "size_" . ucfirst($post->post_name), true);
			switch($pm)
				{case "Small rectangle Box":
					if(count($small_rectangle)<2)
						$small_rectangle[]=$v;
					break;
				case "Small Box":
				case "Small Box With Text":
					if(count($small)<4)
						$small[]=$v;
					break;
				case "Column Banner":
					if(!isset($column))
						$column=$v;
					break;
				}
			}
		$exit=true;
		}
?>
			<div class="minis minis-left">
<?php
	if(isset($small[0]))
		{
?>
				<a <?php echo link_to_follow($small[0]); ?>>
					<div class="zoom">
						<div class="contzoom">
							<img src="<?php echo get_featured($small[0]->ID, "small-box-a"); ?>" class="zooming" />
						</div>
<?php
		$meta=get_post_meta($small[0]->ID, 'size_' . ucfirst($post->post_name));
		if($meta[0]=="Small Box With Text")
			{
?>
					
						<div class="section-text">
							<div class="section-text">
								<div class="mini-title">
									<?php _e($small[0]->post_title); ?>
								</div>
								<div class="mini-body">
									<?php echo get_content($small[0], 180); ?>
								</div>
							</div>
						</div>
<?php		} ?>
					</div>
				</a>
<?php
		}
	if(isset($small[1]))
		{
?>
				<a <?php echo link_to_follow($small[1]); ?>><div class="zoom">
					<div class="contzoom">
						<img src="<?php echo get_featured($small[1]->ID, "small-box-a"); ?>" class="zooming" />
					</div>
<?php
		$meta=get_post_meta($small[1]->ID, 'size_' . ucfirst($post->post_name));
		if($meta[0]=="Small Box With Text")
			{
?>
					
						<div class="section-text">
							<div class="section-text">
								<div class="mini-title">
									<?php _e($small[1]->post_title); ?>
								</div>
								<div class="mini-body">
									<?php echo get_content($small[1], 180); ?>
								</div>
							</div>
						</div>
<?php		} ?>
					</div>
				</a>
<?php
		}
	if(isset($small_rectangle[0]))
		{
?>
				<div class="zoom">
					<div class="contzoom">
						<a <?php echo link_to_follow($small_rectangle[0]); ?>><img src="<?php echo get_featured($small_rectangle[0]->ID, "small-rectangle"); ?>" class="zooming" /></a>
					</div>
				</div>
<?php	} ?>
			</div>
			<div class="minis minis-middle">
<?php
	if(isset($small[2]))
		{
?>
				<a <?php echo link_to_follow($small[2]); ?>>
					<div class="zoom">
						<div class="contzoom">
							<img src="<?php echo get_featured($small[2]->ID, "small-box-a"); ?>" class="zooming" />
						</div>
<?php
		$meta=get_post_meta($small[2]->ID, 'size_' . ucfirst($post->post_name));
		if($meta[0]=="Small Box With Text")
			{
?>
					
						<div class="section-text">
							<div class="section-text">
								<div class="mini-title">
									<?php _e($small[2]->post_title); ?>
								</div>
								<div class="mini-body">
									<?php echo get_content($small[2], 180); ?>
								</div>
							</div>
						</div>
<?php		} ?>
					</div>
				</a>
<?php
		}
	if(isset($small_rectangle[0]))
		{
?>
				<div class="zoom">
					<div class="contzoom">
						<a <?php echo link_to_follow($small_rectangle[1]); ?>><img src="<?php echo get_featured($small_rectangle[1]->ID, "small-rectangle"); ?>" class="zooming" /></a>
					</div>
				</div>
<?php
		}
	if(isset($column))
		{
?>
				<a <?php echo link_to_follow($column); ?>>
					<div class="zoom">
						<div class="contzoom">
							<img src="<?php echo get_featured($column->ID, "column-banner"); ?>" class="zooming" />
						</div>
						<div class="section-text">
							<div class="section-text">
								<div class="mini-title">
									<?php _e($column->post_title); ?>
								</div>
								<div class="mini-body">
									<?php echo get_content($column, 180); ?>
								</div>
							</div>
						</div>
					</div>
				</a>
<?php	} ?>
			</div>
			<div class="minis minis-right">
<?php
	if(isset($small[3]))
		{
?>
				<a <?php echo link_to_follow($small[3]); ?>>
					<div class="zoom">
						<div class="contzoom">
							<img src="<?php echo get_featured($small[3]->ID, "small-box-a"); ?>" class="zooming" />
						</div>
<?php
		$meta=get_post_meta($small[3]->ID, 'size_' . ucfirst($post->post_name));
		if($meta[0]=="Small Box With Text")
			{
?>
					
						<div class="section-text">
							<div class="section-text">
								<div class="mini-title">
									<?php _e($small[3]->post_title); ?>
								</div>
								<div class="mini-body">
									<?php echo get_content($small[3], 180); ?>
								</div>
							</div>
						</div>
<?php		} ?>
					</div>
				</a>
<?php
		}
?>
				<?php include 'get_calendar.php'; ?>
<?php
	if(isset($small[4]))
		{
?>
				
				<div class="zoom">
					<div class="contzoom">
						<a <?php echo link_to_follow($small[4]); ?>><img src="<?php echo get_featured($small[4]->ID, "small-box-a"); ?>" class="zooming" /></a>
					</div>
				</div>
<?php	} ?>
			</div>
		</section>
		<script>
			setup_a();
		</script>
<?php get_footer(); ?>