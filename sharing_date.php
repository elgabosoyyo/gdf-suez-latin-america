<?php
	$fields=get_field_objects($post->ID);
	if(isset($fields["title"]["value"]))
		$title=$fields["title"]["value"];
	
	if(!isset($fields["title"]["value"]) or $title=="")
		$title=$post->post_title;
	
	$date=get_date($post);
?>
			<div class="new-date">
				<div class="new-day">
					<?php echo @$date[2]; ?>
				</div>
				<div class="new-month">
					<?php echo @$date[1]; ?>
				</div>
				<div class="new-year">
					<?php echo @$date[0]; ?>
				</div>
			</div>
			<div class="title-share">
				<h1 class="page-title"><?php echo _e($title); ?></h1>
				<div class="social-share">
					<img src="<?php echo bloginfo("template_url"); ?>/images/facebook.png" onclick="share_social('Facebook')" alt="Facebook" title="Facebook">
					<img src="<?php echo bloginfo("template_url"); ?>/images/google.png" onclick="share_social('Google')" alt="Google+" title="Google+">
					<img src="<?php echo bloginfo("template_url"); ?>/images/tumblr.png" onclick="share_social('Tumblr')" alt="Tumblr" title="tumblr">
					<img src="<?php echo bloginfo("template_url"); ?>/images/twitter.png" onclick="share_social('Twitter')" alt="Twitter" title="Twitter" data-guid="<?php echo $post->ID; ?>">
					<img src="<?php echo bloginfo("template_url"); ?>/images/linkedin.png" onclick="share_social('LinkedIn')" alt="LinkedIn" title="LinkedIn">
					<img src="<?php echo bloginfo("template_url"); ?>/images/more.jpg" onclick="open_social()" alt="<?php _e("[:en]More options[:es]Más opciones[:pt]Mais opções"); ?>" title="<?php _e("[:en]More options[:es]Más opciones[:pt]Mais opções"); ?>"><br />
					<div id="social-more">
						<span onclick="share_social('Digg')"><img src="<?php echo bloginfo("template_url"); ?>/images/diggit.png" alt="Digg" title="Digg"> Digg</span><br />
						<span onclick="share_social('Stumble')"><img src="<?php echo bloginfo("template_url"); ?>/images/stumbleupon.png" alt="StumbleUpon" title="StumbleUpon"> StumbleUpon</span><br />
						<!--<span onclick="share_social('Flattr')"><img src="<?php echo bloginfo("template_url"); ?>/images/flattr.png" alt="Flattr" title="Flattr"> Flattr</span><br />-->
						<span onclick="share_social('Print')"><img src="<?php echo bloginfo("template_url"); ?>/images/print.png" alt="<?php _e("[:en]Print this page[:es]Imprima esta página[:pt]Imprima esta página"); ?>" title="<?php _e("[:en]Print this page[:es]Imprima esta página[:pt]Imprima esta página"); ?>"> <?php _e("[:en]Print this page[:es]Imprima esta página[:pt]Imprima esta página"); ?></span><br />
						<span onclick="share_social('Reddit')"><img src="<?php echo bloginfo("template_url"); ?>/images/reddit.png" alt="Reddit" title="Reddit"> Reddit</span><br />
						<span onclick="share_social('Pinterest')"><img src="<?php echo bloginfo("template_url"); ?>/images/pinterest.png" alt="Pinterest" title="Pinterest"> Pinterest</span><br />
						<a href="mailto:?Subject=www.gdfsuezla.com&Body=<?php echo $_SERVER["SCRIPT_URI"]; ?>"><img src="<?php echo bloginfo("template_url"); ?>/images/email.png" alt="<?php _e("[:en]E-mail[:es]Correo Electrónico[:pt]Correio Electrónico"); ?>" title="<?php _e("[:en]E-mail[:es]Correo Electrónico[:pt]Correio Electrónico"); ?>"> <?php _e("[:en]E-mail[:es]Correo Electrónico[:pt]Correio Electrónico"); ?></a><br />
						<span onclick="share_social('Buffer')"><img src="<?php echo bloginfo("template_url"); ?>/images/buffer.png" alt="Buffer" title="Buffer"> Buffer</span><br />
					</div>
				</div>
			</div>