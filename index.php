<?php
	/* template name: Index */
	get_header();
	$fields=get_field_objects($post->ID);
?>
		<section id="news" data-0="background-position: left 222px top -17%, right 222px bottom 50%;">
			<div class="w1000">
				<div id="big-news">
					<div class="title-big">
						<?php echo $fields["title"]["value"]; ?>
					</div>
					<div class="image-big">
						<div class="img-desc">
							<img src="<?php echo get_featured($post->ID, 'home'); ?>" />
						</div>
						<div class="desc-desc">
							<?php _e($post->post_content); ?>
						<div>
<?php
	$links=explode("<br />",$fields["links"]["value"]);
	foreach($links as $v)
		{$link=explode("|",$v);
		if(count($link)===1)
			break;
		
		$l=__($link[1]);
		if(strpos($l, "http://")===false and strpos($l, "https://")===false)
			$l="http://" . $l;
?>
								<a href="<?php echo $l; ?>"><?php _e($link[0]); ?></a>
<?php	} ?>
							</div>
						</div>
					</div>
<?php
	$pages=get_posts(array(
	'post_type' => array('post', 'page'),
	'sort_column' => 'post_date',
	'orderby' => 'post_date',
	'posts_per_page' => -1,
	'meta_query' => array(
		'relation' => 'OR',
		array(
			'key' => 'box_size_index',
			'value' => 'Large Box',
			'compare' => '='
		),
		array(
			'key' => 'box_size_index',
			'value' => 'Small Box',
			'compare' => '='
		))));
	
	$large=array();
	$small=array();
	foreach($pages as $v)
		{$fields=get_field_objects($v->ID);
		if($fields["box_size_index"]["value"]=="Large Box" and count($large)<3)
			$large[]=$v;
		
		if($fields["box_size_index"]["value"]=="Small Box" and count($small)<3)
			$small[]=$v;
		}
?>
					<div>
						<div class="little-col">
							<a <?php echo link_to_follow($large[0]->ID); ?>>
								<div class="section">
									<div class="img-section">
										<img src="<?php echo @get_featured($large[0]->ID, 'large-box'); ?>" />
									</div>
									<div class="section-text">
										<div class="mini-title">
											<?php @_e($large[0]->post_title); ?>
										</div>
										<div class="mini-body">
											<?php echo @get_content($large[0], 180); ?>
										</div>
									</div>
								</div>
							</a>
							<a <?php echo link_to_follow($small[0]->ID); ?>>
								<div class="section">
									<div class="img-section">
										<img src="<?php echo @get_featured($small[0]->ID, 'small-box'); ?>" />
									</div>
								</div>
							</a>
						</div>
						<div class="little-col">
							<?php include 'get_calendar.php'; ?>
							<a <?php echo link_to_follow($large[1]->ID); ?>>
								<div class="section">
									<div class="img-section">
										<img src="<?php echo @get_featured($large[1]->ID, 'large-box'); ?>" />
									</div>
									<div class="section-text">
										<div class="mini-title">
											<?php @_e($large[1]->post_title); ?>
										</div>
										<div class="mini-body">
											<?php echo @get_content($large[1], 180); ?>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div id="little-col3" class="little-col">
							<a <?php echo link_to_follow($small[1]->ID); ?>>
								<div class="section">
									<div class="img-section">
										<img src="<?php echo @get_featured($small[1]->ID, 'small-box'); ?>" />
									</div>
								</div>
							</a>
							<a <?php echo link_to_follow($small[2]->ID); ?>>
								<div class="section">
									<div class="img-section">
										<img src="<?php echo @get_featured($small[2]->ID, 'small-box'); ?>" />
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>
				<div id="small-news">
					<?php include 'get_press.php'; ?>
					<?php include 'get_news.php'; ?>
					<a <?php echo link_to_follow($large[2]->ID); ?>>
						<div class="section">
							<div class="img-section">
								<img src="<?php echo @get_featured($large[2]->ID, 'large-box'); ?>" />
							</div>
							<div class="section-text">
								<div class="mini-title">
									<?php @_e($large[2]->post_title); ?>
								</div>
								<div class="mini-body">
									<?php echo @get_content($large[2], 180); ?>
								</div>
							</div>
						</div>
					</a>
				</div>
			</div>
		</section>
<?php
	$box=get_posts(array(
	'post_type' => array('post', 'page'),
	'sort_column' => 'post_date',
	'orderby' => 'post_date',
	'posts_per_page' => -1,
	'meta_query' => array(
		'relation' => 'OR',
		array(
			'key' => 'box_size_index_inferior',
			'value' => 'Small Box',
			'compare' => '='
		))));
	
	$banner=get_posts(array(
	'post_type' => array('post', 'page'),
	'sort_column' => 'post_date',
	'orderby' => 'post_date',
	'posts_per_page' => -1,
	'meta_query' => array(
		'relation' => 'OR',
		array(
			'key' => 'box_size_index_inferior',
			'value' => 'Small Banner',
			'compare' => '='
		))));
	
	$column=get_posts(array(
	'post_type' => array('post', 'page'),
	'sort_column' => 'post_date',
	'orderby' => 'post_date',
	'posts_per_page' => 1,
	'meta_query' => array(
		'relation' => 'OR',
		array(
			'key' => 'box_size_index_inferior',
			'value' => 'Column Banner',
			'compare' => '='
		))));
	
	$big=get_posts(array(
	'post_type' => array('post', 'page'),
	'sort_column' => 'post_date',
	'orderby' => 'post_date',
	'posts_per_page' => 1,
	'meta_query' => array(
		'relation' => 'OR',
		array(
			'key' => 'box_size_index_inferior',
			'value' => 'Big Banner',
			'compare' => '='
		))));
?>
		<section id="index-down">
			<div class="w1000">
				<div id="comp-up">
					<a href="<?php echo qtrans_convertURL(get_permalink($big[0]->ID), qtrans_getLanguage()); ?>"><img src="<?php echo get_featured($big[0], "big-banner"); ?>" /></a>
					<a href="<?php echo qtrans_convertURL(get_permalink($column[0]->ID), qtrans_getLanguage()); ?>"><img src="<?php echo get_featured($column[0], "column-banner-index"); ?>" /></a>
				</div>
				<div id="box-banner">
<?php
	foreach($box as $v)
		{
?>
					<a class="box" href="<?php echo qtrans_convertURL(get_permalink($v->ID), qtrans_getLanguage()); ?>" style="background-image: url('<?php echo get_featured($v, "small-box-index"); ?>');"></a>
<?php
		}
	echo "<br /> <br />";
	foreach($banner as $v)
		{
?>
					<a class="banner" href="<?php echo qtrans_convertURL(get_permalink($v->ID), qtrans_getLanguage()); ?>"><img src='<?php echo get_featured($v, "small-banner"); ?>' /><span><div><?php echo get_title($v, 42); ?></div><?php echo get_content($v, 42); ?></span></a>
<?php	} ?>
				</div>
			</div>
		</section>
		<!--<section id="social">
			<div class="w1000">
				<span id="social-title">OUR SOCIAL MEDIA</span><br />
				<span id="social-subtitle">PRESENCE</span>
				<div id="facebook" class="social-net">
					<a target="_blank" href="https://www.facebook.com/gdfsuez"><img src="<?php bloginfo("template_url"); ?>/images/facebook.jpg" id="faceimg" /></a>
					<div class="post show">
						<div class="net-title">gdf suez</div>
						<div class="face-when">1 week ago</div>
						<div class="face-msj">
							Sensibiliser les élèves de collège à mieux utiliser l’énergie et à découvrir les métiers de la cuisine. Avec «Les cantines étoilées», GDF SUEZ complète son dispositif pédagogique «J’apprends l’Energie», destiné aux...
						</div>
						<div class="net-foot"><a class="link-social" href="http://www.facebook.com/gdfsuez"></a></div>
					</div>
					<div class="post show">
						<div class="net-title">gdf suez</div>
						<div class="face-when">2 weeks ago</div>
						<div class="face-msj">
							Ce film présente l'activité Exploration-Production au sein du groupe GDF SUEZ, à travers ses missions, ses implantations internationales, ses partenariats en...
						</div>
						<div class="net-foot"><a class="link-social" href="http://www.facebook.com/gdfsuez"></a></div>
					</div>
					<div class="post show">
						<div class="net-title">gdf suez</div>
						<div class="face-when">2 weeks ago</div>
						<div class="face-msj">
							Deux ans après talents des cités, Linda Senoussaoui concrétise son projet associatif...
						</div>
						<div class="net-foot"><a class="link-social" href="http://www.facebook.com/gdfsuez"></a></div>
					</div>
				</div>
				<div id="twitter" class="social-net">
					<a target="_blank" href="http://twitter.com/gdfsuez"><img src="<?php bloginfo("template_url"); ?>/images/twitter.jpg" /></a>
					<div class="twit show">
						<div class="net-title">gdf suez</div>
						<div class="twit-msj">
							RT <a class="tweet-username" target="_blank" href="https://twitter.com/#!/MIPIMWorld">@MIPIMWorld</a>: Denis Simonneau <a class="tweet-username" target="_blank" href="https://twitter.com/#!/GDFSuez">@GDFSuez</a> - must focus on 4 KEY points in cities: green building, social aspects, mobility, culture <a target="_blank" href="https://twitter.com/#!/search/%23MIPIM">#MIPIM</a>
						</div>
						<div class="net-foot">
							<a class="link-social answer" href="http://www.facebook.com/gdfsuez"></a>
							<a class="link-social retweet" href="http://www.facebook.com/gdfsuez"></a>
							<a class="link-social favorite" href="http://www.facebook.com/gdfsuez"></a>
							<a class="link-social" href="http://www.facebook.com/gdfsuez"></a>
						</div>
					</div>
					<div class="twit show">
						<div class="net-title">gdf suez</div>
						<div class="twit-msj">
							Qu’est-ce que le <a target="_blank" href="https://twitter.com/#!/search/%23biomethane">#biomethane</a> ? <a target="_blank" href="http://t.co/Sf396Woh01">http://t.co/Sf396Woh01</a>
						</div>
						<div class="net-foot">
							<a class="link-social answer" href="http://www.facebook.com/gdfsuez"></a>
							<a class="link-social retweet" href="http://www.facebook.com/gdfsuez"></a>
							<a class="link-social favorite" href="http://www.facebook.com/gdfsuez"></a>
							<a class="link-social" href="http://www.facebook.com/gdfsuez"></a>
						</div>
					</div>
					<div class="twit show">
						<div class="net-title">gdf suez</div>
						<div class="twit-msj">
							Tout savoir sur GDF SUEZ et le <a target="_blank" href="https://twitter.com/#!/search/%23biomethane">#biomethane</a> <a target="_blank" href="http://t.co/XRE4P77yxV">http://t.co/XRE4P77yxV</a>
						</div>
						<div class="net-foot">
							<a class="link-social answer" href="http://www.facebook.com/gdfsuez"></a>
							<a class="link-social retweet" href="http://www.facebook.com/gdfsuez"></a>
							<a class="link-social favorite" href="http://www.facebook.com/gdfsuez"></a>
							<a class="link-social" href="http://www.facebook.com/gdfsuez"></a>
						</div>
					</div>
				</div>
				<div id="youtube" class="social-net">
					<a target="_blank" href="https://youtube.com/gdfsuez"><img src="<?php bloginfo("template_url"); ?>/images/youtube.jpg" /></a>
					<div class="video show">
						<iframe src="https://www.youtube.com/embed/LAzvvBqghnQ" frameborder="0" allowfullscreen></iframe>
					</div>
					<div class="video show">
						<iframe src="https://www.youtube.com/embed/5OW7eRs58uA" frameborder="0" allowfullscreen></iframe>
					</div>
				</div>
				<div id="linkedin" class="social-net">
					<a target="_blank" href="http://www.linkedin.com/company/gdf-suez"><img src="<?php bloginfo("template_url"); ?>/images/linkedin.jpg" /></a>
					<div class="lpost show">
						<div class="net-title">gdf suez</div>
						<div class="l-msj">
							With GDFSUEZ, the young talents can meet on Facebook. The perfect platform to know training offers, university partnerships and competitions.
						</div>
						<div class="net-foot">
							<a class="link-social lshare" href="http://www.facebook.com/gdfsuez"></a>
							<a class="link-social llogo" href="http://www.facebook.com/gdfsuez"></a>
						</div>
					</div>
					<div class="lpost show">
						<div class="net-title">gdf suez</div>
						<div class="l-msj">
							 Chez GDF SUEZ, les jeunes talents se retrouvent aussi sur Facebook. Le lieu parfait pour être informé des offres d'alternance, des actualités avec les Grandes...
						</div>
						<div class="net-foot">
							<a class="link-social lshare" href="http://www.facebook.com/gdfsuez"></a>
							<a class="link-social llogo" href="http://www.facebook.com/gdfsuez"></a>
						</div>
					</div>
					<div class="lpost show">
						<div class="net-title">gdf suez</div>
						<div class="l-msj">
							  L’édition 2014 du Challenge du monde des Grandes Ecoles et Universités, c’est dans deux mois ! Cette année encore, Stéphane Diagana est le...
						</div>
						<div class="net-foot">
							<a class="link-social lshare" href="http://www.facebook.com/gdfsuez"></a>
							<a class="link-social llogo" href="http://www.facebook.com/gdfsuez"></a>
						</div>
					</div>
				</div>
			</div>
		</section>
		<script type="text/javascript" src="http://gdata.youtube.com/feeds/users/gdfsuez/uploads?alt=json-in-script&max-results=2&callback=youtube"></script>-->
		<script>
			setup_index();
		</script>
<?php get_footer(); ?>