<?php
	get_header();
	$fields=get_field_objects($post->ID);
?>
		<section class="w1000 page">
<?php
	if(@$fields["date"]["value"]!="")
		include "sharing_date.php";
	else
		include "sharing.php";
?>
			<?php echo _e(apply_filters('the_content', $post->post_content)); ?>
		</section>
		<script>
		</script>
<?php get_footer(); ?>