<?php
	/* template name: Show Category */
	get_header();
	if(term_exists($post->post_name, 'category')!=null)
		{$posts=get_post_wp($post->post_title, 12, $GLOBALS["page"]);
		$cposts=ceil(count(get_post_wp($post->post_title))/12);
		}

	$parent_tmp = get_post_meta($post->post_parent, '_wp_page_template', true);
	if($parent_tmp=="b.php" or $parent_tmp=="a.php" or $parent_tmp=="a-child.php")
		{
?>
		<div class="contmenu">
			<div class="menuie w1000">
<?php
	$parents=get_post_ancestors($post);
		$parents=array_reverse($parents);
		unset($parents[0]);
		$parents[]=$post->ID;
		$first=true;
		foreach($parents as $p)
			{$args = array(
				'sort_order' => 'ASC',
				'sort_column' => 'post_date',
				'child_of' => $p,
				'parent' => $p,
				'post_type' => 'page',
				'post_status' => 'publish');
			
			$pages=get_pages($args);
			foreach($pages as $i=>$v)
				{echo $i==0 ? "<ul style='color: red'>" : "";
				$li=$first ? "li-first-col":"";
				if((in_array($v->ID, $parents) or $v->ID==$post->ID) and !$first)
					$li="li-active";
?>
					<li class="<?php echo $li; ?>"><a target="_self" href="<?php echo get_permalink($v->ID); ?>" class="<?php echo $v->ID==$post->ID ? "active" : ""; ?>"><?php _e($v->post_title); ?></a></li>
<?php			} ?>
				</ul>
<?php
			$first=false;
			}
?>
			</div>
		</div>
<?php	} ?>
		<section class="post-page w1000">
<?php
	include "sharing.php";
	_e(apply_filters('the_content', $post->post_content));
	if(isset($posts))
		{
?>
			<div>
<?php
		$fields=get_field_objects($post->ID);
		if($fields["layout"]["value"]=="Mosaic")
			{foreach($posts as $v)
				{if($v->ID==$post->ID)
					continue;
				if(qtrans_getLanguage()=="en")
					{$img=get_featured($v->ID, "featured");
					$img='<img src="' . $img . '" />';
					}
				else
					$img="";
?>
				<a <?php echo link_to_follow($v); ?>>
					<div class="cont-post" style="<?php if(qtrans_getLanguage()!="en") echo 'height: 171px;'?>">
						<span class="post-title"><?php _e($v->post_title); ?></span>
						<?php echo $img; ?>
						<ul><li><?php echo get_content($v, "", 111); ?></li></ul>
					</div>
				</a>
<?php			} ?>
			</div>
<?php
			}
		else
			{foreach($posts as $v)
				{$date=get_date($v);
				if($v->ID==$post->ID)
					continue;
?>
			<a <?php echo link_to_follow($v); ?>>
				<div class="list-np">
					<div class="new-date">
						<div class="new-day">
							<?php echo @$date[2]; ?>
						</div>
						<div class="new-month">
							<?php echo @$date[1]; ?>
						</div>
						<div class="new-year">
							<?php echo @$date[0]; ?>
						</div>
					</div>
					<div class="img-np">
						<?php echo get_the_post_thumbnail($v->ID, "list-np"); ?>
					</div>
					<div class="prev-np">
						<div class="prev-np-title">
							<?php echo get_title($v,112); ?>
						</div>
						<div class="prev-np-text">
<?php
	$content=apply_filters('the_content', get_content($v));
	dump(substr($content, strpos($content, "<p>"), strpos($content, "</p>")+4));
	echo strip_tags(substr($content, strpos($content, "<p>"), strpos($content, "</p>")+4));
?>
						</div>
					</div>
				</div>
			</a>
<?php
				}
			}
		}
?>
			<div id="pagination">
				<a href="<?php echo site_url() . "/" . qtrans_getLanguage() . "/" . $post->post_name . "/" . $GLOBALS["page"]-1; ?>/" style="<?php echo $GLOBALS["page"]>1 ? "display: block;":""; ?>" class="prev"></a>
				<a href="<?php echo site_url() . "/" . qtrans_getLanguage() . "/" . $post->post_name; ?>/1/" class="<?php echo $GLOBALS["page"]==1 ? "active":""; ?>">1</a>
				<div style="<?php echo $GLOBALS["page"]>6 ? "display: block;":""; ?>" class="ppp">...</div>
<?php
			$cont=0;
			for($c=$GLOBALS["page"]-6>0 ? $GLOBALS["page"]-6+2:2;$c<$cposts/12;$c++)
				{if($cont==9)
					break;
?>
				<a href="<?php echo site_url() . "/" . qtrans_getLanguage() . "/" . $post->post_name . "/" . $c; ?>/" class="<?php echo $GLOBALS["page"]==$c ? "active":""; ?>"><?php echo $c; ?></a>
<?php
				$cont++;
				}
			
			if($cposts==1)
				$c=1;
?>
				<div style="<?php echo $c<$cposts && $cposts>9 ? "display: block;":""; ?>" class="ppp">...</div>
				<a href="<?php echo site_url() . "/" . qtrans_getLanguage() . "/" . $post->post_name . "/" . $cposts; ?>/" style="<?php echo $cposts>1 ? "display: block;":"display: none;"; ?>" class="<?php echo $GLOBALS["page"]==$cposts ? "active":""; ?>"><?php echo $cposts; ?></a>
				<a href="<?php echo site_url() . "/" . qtrans_getLanguage() . "/" . $post->post_name . "/" . $cposts; ?>/" style="<?php echo $GLOBALS["page"]>$cposts ? "display: block;":""; ?>" class="next"></a>
			</div>
		</section>
		<script>
			
		</script>
<?php get_footer(); ?>