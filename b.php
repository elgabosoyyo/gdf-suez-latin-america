<?php
	/* template name: Menu B */
	get_header();
?>
		<div class="contmenu">
			<div class="menuie w1000">
<?php
		$parents=get_post_ancestors($post);
		$parents=array_reverse($parents);
		unset($parents[0]);
		$parents[]=$post->ID;
		$first=true;
		foreach($parents as $p)
			{$args = array(
				'sort_order' => 'ASC',
				'sort_column' => 'post_date',
				'child_of' => $p,
				'parent' => $p,
				'post_type' => 'page',
				'post_status' => 'publish');
			
			$pages=get_pages($args);
			foreach($pages as $i=>$v)
				{echo $i==0 ? "<ul>" : "";
				$li=$first ? "li-first-col":"";
				if((in_array($v->ID, $parents) or $v->ID==$post->ID) and !$first)
					$li="li-active";
?>
					<li class="<?php echo $li; ?>"><a target="_self" href="<?php echo get_permalink($v->ID); ?>" class="<?php echo $v->ID==$post->ID ? "active" : ""; ?>"><?php _e($v->post_title); ?></a></li>
<?php			} ?>
				</ul>
<?php
			$first=false;
			}
?>
			</div>
		</div>
		<section class="w1000 b">
			
<?php
				include "sharing.php";
				_e(apply_filters('the_content', $post->post_content));
				$gallery=get_post_galleries($post, false);
				$images="";
				if(count($gallery))
					{$gallery=explode(",", $gallery[0]["ids"]);
					$primer=true;
					$sizes=Array("three-one", "three-two", "three-three");
					foreach($gallery as $i=>$v)
						{if(!$primer)
							$images.=",";
						$tmp=wp_get_attachment_image_src($v, $sizes[$i]);
						$images.='"' . $tmp[0] . '"';
						$primer=false;
						}
					}
			?>
		</section>
		<script>
<?php
	if($images!="")
		{
?>
			images=Array(<?php echo $images; ?>);
			$(".gallery").attr("class", "").attr("data-carousel-extra", "").attr("id", "three-images").empty();
			$(images).each(function(index, element)
				{$("#three-images").append($('<img>').attr('src', element));
				});
<?php	} ?>
		</script>
		<div class="bdivisor divisor"></div>
<?php get_footer(); ?>