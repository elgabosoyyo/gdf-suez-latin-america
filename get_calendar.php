	<div class="section calendar">
		<div class="section-text">
			<a href="<?php echo site_url(); ?>/calendar">
				<div class="mini-title">
					<?php _e("[:en]calendar[:es]calendario[:pt]calendário"); ?>
				</div>
			</a>
			<div class="mini-body">
<?php
	$npc=get_post_wp("calendar", 2);
	foreach($npc as $v)
	{$date=get_date($v);
?>
				<div class="new-body">
					<div class="new-date">
						<div class="new-day">
							<?php echo @$date[2]; ?>
						</div>
						<div class="new-month">
							<?php echo @$date[1]; ?>
						</div>
						<div class="new-year">
							<?php echo @$date[0]; ?>
						</div>
					</div>
					<div class="new-info">
						<?php echo get_title($v, "", 46); ?>
					</div>
				</div>
<?php	} ?>
			</div>
		</div>
	</div>