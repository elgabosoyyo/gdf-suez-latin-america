var httpRequest, chrome, mobile, w, menu=false, sub=false;

if (window.XMLHttpRequest)
	httpRequest = new XMLHttpRequest();
else if (window.ActiveXObject)
	httpRequest = new ActiveXObject("MSXML2.XMLHTTP");
else
	httpRequest = new ActiveXObject("Microsoft.XMLHTTP");

iexplorer = false;
if(/MSIE/i.test(navigator.userAgent))
    {iexplorer = parseInt(navigator.userAgent.substr(navigator.userAgent.indexOf("MSIE")+5,1));
	if(iexplorer==1)
		iexplorer=10;
	}
	
if(!iexplorer)
	if(/.NET/i.test(navigator.userAgent))
		iexplorer=11;

if(iexplorer<=9 && iexplorer!=false)
    $('head').append($('<link>').attr('href', dir + '/css/lteie9.css').attr("type", 'text/css').attr("rel", 'stylesheet'));

if(/Chrome/i.test(navigator.userAgent))
    chrome = true;

if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))
    mobile = true;

if(chrome && mobile)
	{$('head').append($('<link>').attr('href', dir + '/css/chrome.css').attr("type", 'text/css').attr("rel", 'stylesheet'));
	$('head').append($('<link>').attr('href', dir + '/css/chromemobile.css').attr("type", 'text/css').attr("rel", 'stylesheet'));
	}

if(mobile && !chrome)
    $('head').append($('<link>').attr('href', dir + '/css/mobile.css').attr("type", 'text/css').attr("rel", 'stylesheet'));

if(!mobile && chrome)
    $('head').append($('<link>').attr('href', dir + '/css/chrome.css').attr("type", 'text/css').attr("rel", 'stylesheet'));

function flotante(arg)
	{if(arg==="")
		{hora=new Date().getHours();
		minutos=new Date().getMinutes();
		segundos=new Date().getSeconds();
		
		if(parseInt(hora)<=9)
			hora="0"+hora;
		
		if(parseInt(minutos)<=9)
			minutos="0"+minutos;
		
		if(parseInt(segundos)<=9)
			segundos="0"+segundos;
		texto=hora + ":" + minutos + ":" + segundos;
		}
	else
		texto=arg;
	document.getElementById("flotante").innerHTML=texto;
	}

function flotante2(arg)
	{document.getElementById("flotante").innerHTML=document.getElementById("flotante").innerHTML + " // " + arg;
	}

$(document).ready(function()
	{if(iexplorer<=7 && iexplorer!=false)
		$('#warning').css("display","block");
	
	$(".menu").css("left", $(".w1000:not(.menu)").offset().left + "px");
	
	$(window).resize(function()
		{$("#languages").css("left",$("#lang").offset().left).css("top",$("#lang").offset().top+$("#lang").height()+10)
		
		$(".menu").css("left", $(".w1000:not(.menu)").offset().left + "px");
		if($(window).width()>913 && iexplorer>9)
			{$("body").attr("class","");
			$(".menu-float").removeClass("visible").width(0);
			}
		});
	
	$("iframe").each(function()
		{if($(this).attr("src").indexOf("youtube")==-1 && $(this).attr("src").indexOf("vimeo")==-1)
			$(this).height(screen.height).width("100%");
		
		if($(this).attr("src").indexOf("youtube")!=-1 || $(this).attr("src").indexOf("vimeo")!=-1)
			$(this).height(500).width("100%").attr("width","").attr("height","");
		});
	
	$("#languages").css("left",$("#lang").offset().left).css("top",$("#lang").offset().top+$("#lang").height()+10)
	
	$("#lang").click(function()
		{$("#languages").toggleClass("opened");
		});
	
	$('a[target=_blank]').click(function(event)
		{event.preventDefault();
		window.open($(this).attr("href"),"_blank","width=" + $(window).width() + ",height=" + $(window).height() + ",location=yes, menubar=yes, resizable=yes, scrollbars=yes, status=yes, titlebar=yes, toolbar=yes");
		});
	
	$(".hamburger, .menu-right").click(function()
		{id=$(this).attr("data");
		if(!$("#" + id).hasClass("visible"))
			{$(".menu-float").removeClass("visible").width(0);
			$("#" + id).addClass("visible").width(240);
			$("body").attr("class", "").addClass(id);
			}
		else
			{$(".menu-float").removeClass("visible").width(0);
			$("body").attr("class", "");
			}
		});
	
	$(".menu-float").niceScroll(
		{background: '#6557AF',
		cursorcolor: '#43358C',
		cursorwidth: '5px',
		cursorborder: '2px solid #00AAE7',
		cursorborderradius: '6px',
		enablemousewheel: 'false',
		railalign: "left",
		horizrailenabled: false
		});
    
	$("[data-deploy]:not(.subleft)").mouseenter(function()
		{e=$("[data-id='" + $(this).attr("data-deploy") + "']");
		if(!e.length)
			level=parseInt($(this).parent().attr("data-level"))+1;
		else
			level=parseInt(e.attr("data-level"));
		while($("[data-level='" + level.toString() + "']").length)
			{$("[data-level='" + level.toString() + "']").removeClass("visible");
			level++;
			}
		e.addClass("visible");
		});
    
	$(".subleft[data-deploy]").mouseenter(function()
		{e=$("[data-id='" + $(this).attr("data-deploy") + "']");
		if(!e.hasClass("deployed"))
			{$(".deployed").stop().animate(
				{height: "0px"
				}).removeClass("deployed");
			e.height("auto");
			h=e.height();
			e.height(0);
			e.stop().animate(
				{height: h.toString() + "px"
				},300).addClass("deployed").height("auto");
			}
		});
	
	$("#sub-menu").mouseenter(function()
		{sub=true;
		}).mouseleave(function()
			{sub=false;
			});
	
	$(".menu").mouseenter(function()
		{menu=true;
		}).mouseleave(function()
			{menu=false;
			});
	
	$("#sub-menu, .menu").mouseleave(function()
		{element=this;
		setTimeout(function()
			{if(!menu && !sub)
				{$(".menu").removeClass("deployed").stop().animate(
					{height: "0px"
					},300);
				$("ul.visible").removeClass("visible");
				}
			},100);
		});
	});

function plusMinus(element)
	{if($(element).hasClass("plus"))
		{ul=$("#" + $(element).attr("data-id") + ">ul");
		ul.height("auto");
		h=ul.height();
		ul.height(0).stop().animate(
			{height: h
			},300,function()
				{ul.height("auto");
				$(".menu-float").getNiceScroll().resize();
				});
		
		$(element).removeClass("plus").addClass("minus");
		}
	else
		{$("#" + $(element).attr("data-id") + " ul").stop().animate(
			{height: 0
			},300,function()
				{$(".menu-float").getNiceScroll().resize();
				});
		$("#" + $(element).attr("data-id") + " span").removeClass("minus").addClass("plus");
		}
	}

function setup_index()
	{$("#news").attr("data-" + $("#index-down").offset().top/4, "background-position: left 222px top 40%, right 222px bottom 40%;").attr("data-" + ($("#index-down").offset().top/4)*3, "background-position: left 222px top 50%, right 222px bottom 0%;").attr("data-" + $("#index-down").offset().top, "background-position: left 222px top 50%, right 222px bottom -31%;");
	$("#index-down").attr("data-" + $("#index-down").offset().top, "background-position: left -57%;").attr("data-" + ($("#index-down").offset().top + $("#index-down").height()/2), "background-position: left 110%;");
	
	if(!mobile)
		skrollr.init(
			{forceHeight: false
			});
	
	/*$(".face-msj").niceScroll(
		{background: 'rgba(0,0,0,0)',
		cursorcolor: '#C0BEBF',
		cursorwidth: '7px',
		cursorborder: '0px none',
		cursorborderradius: '6px',
		enablemousewheel: 'false'
		});*/
	
	$(window).resize(function()
		{resize_main();
		});
	
	$(".image-big").mouseover(function()
		{if(chrome)
			w=735;
		else
			w=733;
		
		if($(window).width()>w)
			{$(".desc-desc").stop().animate(
				{marginTop: (-($(".desc-desc").height()+40+(6*$(".image-big").height()/100)*2)).toString() + "px"
				},300);
			}
		}).mouseout(function()
			{$(".desc-desc").stop().animate(
				{marginTop: "0px"
				},300);
			});
	
	$(".section").mouseover(function()
		{$(this).addClass("hover");
		}).mouseout(function()
			{$(this).removeClass("hover");
			})
	
	$(".dossier").mouseover(function()
		{$(this).children("a").css("display", "inline");
		}).mouseout(function()
			{$(this).children("a").css("display", "none");
			});
	
	$(".banner").mouseover(function()
		{$(this).addClass("hover");
		}).mouseout(function()
			{$(this).removeClass("hover");
			});
	
	$(".banner").load(function()
		{$(this).height($(this).children("img").height());
		})
	
	resize_main();
	
	//data=twitter("3");
	}

function resize_main()
	{$(".image-big").height($(".image-big img").height());
	
	$(this).height($(this).children("img").height());
	
	/*if(chrome)
		h=735;
	else
		h=733;
	
	if($(window).width()<=h)
		{$(".img-section").each(function()
			{img=$(this).children("img");
			$(img).css("margin-left", (($(this).width()-$(img).width())/2).toString() + "px");
			});
		
		$(".video").height($(".video").width()-250);
		}
	else
		{$(".video").height($("#faceimg").height());
		$(".img-section").each(function()
			{$(this).children("img").css("margin-left", "0px");
			});
		}*/
	}

function setup_a()
	{/*$(".slider").each(function(index)
		{h1=$(this).children("img").height(),
		h2=$(this).children(".new-black").children(".slider-title").height();
		$(this).children(".new-black").height(h1);
		$(this).children(".new-black").children(".slider-desc").height(h1-h2-60);
		});*/
	
	$(".new-black").each(function()
		{$(this).children(".slider-desc").height(235-$(this).children(".slider-title").height()-40);
		}).height($("#cicle").height());
	
	$('#cicle').cycle(
		{after: function(currSlideElement, nextSlideElement, options, forwardFlag)
			{$(".slider-desc").getNiceScroll().resize();
			},
		fx:	'scrollLeft',
		timeout:	10000,
		pager:	'#pager',
		pause:	1,
		pagerAnchorBuilder: function(idx, slide)
			{return ('<div class="subpager"><div></div></div>');
			}
		});
	
	$(".slider>img").css("height", $("#cicle").height().toString()+"px");
	
	$(".slider-desc").niceScroll(
		{background: 'rgba(0,0,0,0)',
		cursorcolor: '#C0BEBF',
		cursorwidth: '7px',
		cursorborder: '0px none',
		cursorborderradius: '6px',
		enablemousewheel: 'false'
		});
	
	$(".zoom, .calendar").mouseover(function()
		{$(this).addClass("hover");
		}).mouseout(function()
			{$(this).removeClass("hover");
			});
	
	$(window).resize(function()
		{$(".slider>img").css("height", $("#cicle").height().toString()+"px");
		});
	}

function youtube(data)
	{var feed = data.feed;
	var entries = feed.entry || [];
	var html = [];
	for(var i = 0; i<entries.length; i++)
		{var entry = entries[i];
		var playerUrl = entries[i].media$group.media$content[0].url;
		
		$('#youtube.social-net').append($('<div>').addClass("video").addClass("show").append($("<iframe>").attr("src", 'https://www.youtube.com/embed/' + playerUrl.substr(playerUrl.indexOf("/v/")+3,playerUrl.indexOf("?")-playerUrl.indexOf("/v/")) + '?rel=0').attr("frameborder","0").attr("allowfullscreen", "")));
		}
	}

function twitter(count)
	{$.getJSON("/new/wp-content/themes/GDF The Amalgama/get_tweets.php?count=" + count, function(data)
		{return data;
		}).fail(function()
			{console.log("fail");
			});
	}

function share_social(arg)
	{switch(arg)
		{case "Google":
			url="https://plus.google.com/share?url=" + encodeURIComponent(location.href);
			size="width=475,height=340";
			break;
		case "Digg":
			url="http://www.digg.com/submit?url=" + encodeURIComponent(location.href);
			size="width=900,height=315";
			break;
		case "LinkedIn":
			url="http://www.linkedin.com/shareArticle?mini=true&url=" + encodeURIComponent(location.href);
			size="width=603,height=423";
			break;
		case "Stumble":
			url="http://www.stumbleupon.com/submit?url=" + encodeURIComponent(location.href)+ "&title=" + encodeURIComponent(title);
			size="width=897,height=535";
			break;
		/*case "Flattr":
			url="https://flattr.com/submit/auto?user_id=&url=" + encodeURIComponent(location.href);
			break;*/
		case "Print":
			url="";
			print();
			break;
		case "Facebook":
			url="http://www.facebook.com/sharer.php?u=" + encodeURIComponent(location.href);
			size="width=800,height=300";
			break;
		case "Reddit":
			url="http://reddit.com/submit?url=" + encodeURIComponent(location.href);
			size="width=900,height=500";
			break;
		case "Tumblr":
			url="http://www.tumblr.com/share/link?url=" + encodeURIComponent(location.href) + "&name=" + encodeURIComponent(title) + "&description=";
			size="width=440,height=445";
			break;
		case "Twitter":
			url="http://twitter.com/share?text=" + titlet + "&url=" + encodeURIComponent(urls + "/?p=" + $("[title=Twitter]").attr("data-guid"));
			size="width=800,height=260";
			break;
		case "Pinterest":
			url="http://pinterest.com/pin/create/bookmarklet/?media=&url=" + encodeURIComponent(location.href) + "&is_video=false&description=" + encodeURIComponent(title);
			size="width=750,height=310";
			break;
		case "Buffer":
			url="https://bufferapp.com/add?url=" + encodeURIComponent(location.href) + "&text=" + encodeURIComponent(title);
			size="width=566,height=573";
			break;
		}
	
	if(url!="")
		w=window.open(url,"_blank",size);
	}

function open_social()
	{$("#social-more").toggleClass("opened");
	}

function setup_news()
	{$("#bydate, #bytheme").click(function()
		{$("#bydate, #bytheme").children("span").addClass("alpha03");
		$(this).children("span").removeClass("alpha03");
		$("#search").val($(this).attr("id"));
		});
	
	$("#bydate, #bytheme").children("select").each(function()
		{changenew(this);
		});
	}

function changenew(arg)
	{$("[data-id=" + $(arg).attr("id") + "]").html($(arg).children("option:selected").text());
	}

function checknew()
	{id="#" + ($(".alpha03").length==4 ? "bytheme":"bydate");
	ans=true
	$(id).children("select").each(function()
		{if($(this).val()==="00")
			ans=false;
		})
	return ans;
	}

/*lang = window.navigator.userLanguage || window.navigator.language;
httpRequest.onreadystatechange=function()
	{if(httpRequest.readyState==4)
		{httpRequest.responseText;
		}
	}
httpRequest.open('post','lang.php',false);
httpRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
httpRequest.send("lang=" + lang);*/