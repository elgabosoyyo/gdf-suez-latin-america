	<div class="new">
		<a href="<?php echo site_url(); ?>/news">
			<div class="new-title">
				<?php _e("[:en]news[:es]noticias[:pt]noticias"); ?>
			</div>
		</a>
		<div class="mini-body">
<?php
	$npc=get_post_wp("news", 2);
	foreach($npc as $v)
		{$date=get_date($v);
?>
			<a href="<?php echo qtrans_convertURL(get_permalink($v->ID), qtrans_getLanguage()) ?>">
				<div class="new-body">
					<div class="new-info">
						<?php echo get_title($v, 58); ?>
					</div>
					<div class="new-date">
						<div class="new-day">
							<?php echo @$date[2]; ?>
						</div>
						<div class="new-month">
							<?php echo @$date[1]; ?>
						</div>
						<div class="new-year">
							<?php echo @$date[0]; ?>
						</div>
					</div>
				</div>
			</a>
<?php	} ?>
		</div>
	</div>