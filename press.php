<?php
	/* template name: Press */
	get_header();
	
	$news=get_post_wp("press", 10, $GLOBALS["page"]-1);
	$pages=ceil(count(get_post_wp("press"))/10);
?>
		<section id="np" class="w1000">
			<form id="new-search" action="<?php echo site_url(); ?>/<?php echo strtolower($post->post_title); ?>/1" method="post" onsubmit="return checknew()">
				<span><div id="s"><?php _e("[:es]BUSCAR[:en]SEARCH[:pt]PESQUISA"); ?></div></span>
				<span>
					<div id="byd"><?php _e("[:es]POR[:en]BY[:pt]POR"); ?><br /><span><?php _e("[:es]FECHA[:en]DATE[:pt]DATA"); ?></span></div>
					<div id="bydate">
						<?php
							_e("[:es]Desde[:en]From[:pt]Do");
							$tmp="";
							if(isset($_POST["monthf"]))
								$tmp=$_POST["monthf"];
							else if(isset($_SESSION["monthf"][$post->post_tilte]))
								$tmp=$_SESSION["monthf"][$post->post_tilte];
						?>
						<span class="select" data-id="monthf"><?php _e("[:es]Meses[:en]Month[:pt]Meses"); ?></span>
						<select id="monthf" name="monthf" onchange="changenew(this)">
							<option value="00"<?php echo $tmp=="00" ? " selected":""; ?>><?php _e("[:es]Meses[:en]Month[:pt]Meses"); ?></option>
							<option value="01"<?php echo $tmp=="01" ? " selected":""; ?>><?php _e("[:es]Enero[:en]January[:pt]Janeiro"); ?></option>
							<option value="02"<?php echo $tmp=="02" ? " selected":""; ?>><?php _e("[:es]Febrero[:en]February[:pt]Fevereiro"); ?></option>
							<option value="03"<?php echo $tmp=="03" ? " selected":""; ?>><?php _e("[:es]Marzo[:en]March[:pt]Março"); ?></option>
							<option value="04"<?php echo $tmp=="04" ? " selected":""; ?>><?php _e("[:es]Abril[:en]April[:pt]Abril"); ?></option>
							<option value="05"<?php echo $tmp=="05" ? " selected":""; ?>><?php _e("[:es]Mayo[:en]May[:pt]Maio"); ?></option>
							<option value="06"<?php echo $tmp=="06" ? " selected":""; ?>><?php _e("[:es]Junio[:en]June[:pt]Junho"); ?></option>
							<option value="07"<?php echo $tmp=="07" ? " selected":""; ?>><?php _e("[:es]Julio[:en]July[:pt]Julho"); ?></option>
							<option value="08"<?php echo $tmp=="08" ? " selected":""; ?>><?php _e("[:es]Agosto[:en]August[:pt]Agosto"); ?></option>
							<option value="09"<?php echo $tmp=="09" ? " selected":""; ?>><?php _e("[:es]Septiembre[:en]September[:pt]Setembro"); ?></option>
							<option value="10"<?php echo $tmp=="10" ? " selected":""; ?>><?php _e("[:es]Octubre[:en]October[:pt]Outubro"); ?></option>
							<option value="11"<?php echo $tmp=="11" ? " selected":""; ?>><?php _e("[:es]Noviembre[:en]November[:pt]Novembro"); ?></option>
							<option value="12"<?php echo $tmp=="12" ? " selected":""; ?>><?php _e("[:es]Diciembre[:en]December[:pt]Dezembro"); ?></option>
						</select>
						<span class="select" data-id="yearf"><?php _e("[:es]Año[:en]Year[:pt]Ano"); ?></span>
						<select id="yearf" name="yearf" onchange="changenew(this)">
							<option value="00"<?php echo @$_POST["yearf"]=="00" ? " selected":""; ?>><?php _e("[:es]Año[:en]Year[:pt]Ano"); ?></option>
<?php
	$tmp="";
	if(isset($_POST["yearf"]))
		$tmp=$_POST["yearf"];
	else if(isset($_SESSION["yearf"][$post->post_tilte]))
		$tmp=$_SESSION["yearf"][$post->post_tilte];
	
	for($c=2011;$c<=date("Y");$c++)
		{
?>
							<option value="<?php echo $c; ?>"<?php echo $tmp==$c ? " selected":""; ?>><?php echo $c; ?></option>
<?php	} ?>
						</select>
						<br />
						<?php
							_e("[:es]Hasta[:en]To[:pt]Para");
							$tmp="";
							if(isset($_POST["montht"]))
								$tmp=$_POST["montht"];
							else if(isset($_SESSION["montht"][$post->post_tilte]))
								$tmp=$_SESSION["montht"];
						?>
						<span class="select" data-id="montht"><?php _e("[:es]Meses[:en]Month[:pt]Meses"); ?></span>
						<select id="montht" name="montht" onchange="changenew(this)">
							<option value="00"<?php echo $tmp=="00" ? " selected":""; ?>><?php _e("[:es]Meses[:en]Month[:pt]Meses"); ?></option>
							<option value="01"<?php echo $tmp=="01" ? " selected":""; ?>><?php _e("[:es]Enero[:en]January[:pt]Janeiro"); ?></option>
							<option value="02"<?php echo $tmp=="02" ? " selected":""; ?>><?php _e("[:es]Febrero[:en]February[:pt]Fevereiro"); ?></option>
							<option value="03"<?php echo $tmp=="03" ? " selected":""; ?>><?php _e("[:es]Marzo[:en]March[:pt]Março"); ?></option>
							<option value="04"<?php echo $tmp=="04" ? " selected":""; ?>><?php _e("[:es]Abril[:en]April[:pt]Abril"); ?></option>
							<option value="05"<?php echo $tmp=="05" ? " selected":""; ?>><?php _e("[:es]Mayo[:en]May[:pt]Maio"); ?></option>
							<option value="06"<?php echo $tmp=="06" ? " selected":""; ?>><?php _e("[:es]Junio[:en]June[:pt]Junho"); ?></option>
							<option value="07"<?php echo $tmp=="07" ? " selected":""; ?>><?php _e("[:es]Julio[:en]July[:pt]Julho"); ?></option>
							<option value="08"<?php echo $tmp=="08" ? " selected":""; ?>><?php _e("[:es]Agosto[:en]August[:pt]Agosto"); ?></option>
							<option value="09"<?php echo $tmp=="09" ? " selected":""; ?>><?php _e("[:es]Septiembre[:en]September[:pt]Setembro"); ?></option>
							<option value="10"<?php echo $tmp=="10" ? " selected":""; ?>><?php _e("[:es]Octubre[:en]October[:pt]Outubro"); ?></option>
							<option value="11"<?php echo $tmp=="11" ? " selected":""; ?>><?php _e("[:es]Noviembre[:en]November[:pt]Novembro"); ?></option>
							<option value="12"<?php echo $tmp=="12" ? " selected":""; ?>><?php _e("[:es]Diciembre[:en]December[:pt]Dezembro"); ?></option>
						</select>
						<span class="select" data-id="yeart"><?php _e("[:es]Año[:en]Year[:pt]Ano"); ?></span>
						<select id="yeart" name="yeart" onchange="changenew(this)">
							<option value="00"<?php echo @$_POST["yeart"]=="00" ? " selected":""; ?>><?php _e("[:es]Año[:en]Year[:pt]Ano"); ?></option>
<?php
	$tmp="";
	if(isset($_POST["yeart"]))
		$tmp=$_POST["yeart"];
	else if(isset($_SESSION["yeart"][$post->post_tilte]))
		$tmp=$_SESSION["yeart"];
	
	for($c=2011;$c<=date("Y");$c++)
		{
?>
							<option value="<?php echo $c; ?>"<?php echo $tmp==$c ? " selected":""; ?>><?php echo $c; ?></option>
<?php	} ?>
						</select>
					</div>
				</span>
				<span>
					<!--<div id="byt"><?php _e("[:es]POR[:en]BY[:pt]POR"); ?><br /><span><?php _e("[:es]TEMA[:en]THEME[:pt]TEMA"); ?></span></div>
					<div id="bytheme">
						<span class="select" data-id="monthth"><?php _e("[:es]Tema[:en]Theme[:pt]Tema"); ?></span>
						<select id="monthth" name="monthth" onchange="changenew(this)">
							<option value="00"<?php echo @$_POST["monthth"]=="00" ? " selected":""; ?>><?php _e("[:es]Tema[:en]Theme[:pt]Tema"); ?></option>
							<option value="01"<?php echo @$_POST["monthth"]=="01" ? " selected":""; ?>><?php _e("[:es]Enero[:en]January[:pt]Janeiro"); ?></option>
						</select><br />
						<span class="select" data-id="yearth"><?php _e("[:es]Año[:en]Year[:pt]Ano"); ?></span>
						<select id="yearth" name="yearth" onchange="changenew(this)">
							<option value="00"<?php echo @$_POST["yearth"]=="00" ? " selected":""; ?>><?php _e("[:es]Año[:en]Year[:pt]Ano"); ?></option>
<?php
	for($c=2011;$c<=date("Y");$c++)
		{
?>
							<option value="<?php echo $c; ?>"<?php echo @$_POST["yearth"]==$c ? " selected":""; ?>><?php echo $c; ?></option>
<?php	} ?>
						</select>
					</div>-->
					<input type="submit" value="<?php _e("[:es]BUSCAR[:en]SEARCH[:pt]PESQUISA"); ?>" />
				</span>
				<input type="hidden" name="search" id="search" />
			</form>
<?php
	if(isset($_SESSION["search"][$post->post_tilte]) || isset($_POST["search"]))
		{if(isset($_POST["search"]))
			{$news=get_posts_date(array(9,229), 10, $GLOBALS["page"]-1);
			$pages=ceil(count(get_posts_date(array(9,229)))/10);
			}
		else
			{$news=get_posts_date(array(9,229), 10, $GLOBALS["page"]-1);
			$pages=ceil(count(get_posts_date(array(9,229)))/10);
			}
		}
	else
		{$news=get_post_wp("news", 10, $GLOBALS["page"]-1);
		$pages=ceil(count(get_post_wp("news"))/10);
		}
	
	foreach($news as $v)
		{$date=get_date($v);
?>
			<a <?php echo link_to_follow($v); ?>>
				<div class="list-np">
					<div class="new-date">
						<div class="new-day">
							<?php echo @$date[2]; ?>
						</div>
						<div class="new-month">
							<?php echo @$date[1]; ?>
						</div>
						<div class="new-year">
							<?php echo @$date[0]; ?>
						</div>
					</div>
					<div class="prev-np">
						<div class="prev-np-title">
							<?php echo get_title($v); ?>
						</div>
					</div>
				</div>
			</a>
<?php	} ?>
		<div id="pagination">
			<a href="<?php echo site_url(); ?>/<?php echo qtrans_getLanguage(); ?>/press/<?php echo $GLOBALS["page"]-1; ?>/" style="<?php echo $GLOBALS["page"]>1 ? "display: block;":""; ?>" class="prev"></a>
			<a href="<?php echo site_url(); ?>/<?php echo qtrans_getLanguage(); ?>/press/1/" class="<?php echo $GLOBALS["page"]==1 ? "active":""; ?>">1</a>
			<div style="<?php echo $GLOBALS["page"]>6 ? "display: block;":""; ?>" class="ppp">...</div>
<?php
		$cont=0;
		for($c=$GLOBALS["page"]-6>0 ? $GLOBALS["page"]-6+2:2;$c<$pages;$c++)
			{if($cont==9)
				break;
?>
			<a href="<?php echo site_url(); ?>/<?php echo qtrans_getLanguage(); ?>/press/<?php echo $c; ?>/" class="<?php echo $GLOBALS["page"]==$c ? "active":""; ?>"><?php echo $c; ?></a>
<?php
			$cont++;
			}
		
		if($pages==1)
			$c=1;
?>
			<div style="<?php echo $c<$pages && $pages>9 ? "display: block;":""; ?>" class="ppp">...</div>
			<a href="<?php echo site_url(); ?>/<?php echo qtrans_getLanguage(); ?>/press/<?php echo $pages; ?>/" style="<?php echo $pages>1 ? "display: block;":"display: none;"; ?>" class="<?php echo $GLOBALS["page"]==$pages ? "active":""; ?>"><?php echo $pages; ?></a>
			<a href="<?php echo site_url(); ?>/<?php echo qtrans_getLanguage(); ?>/press/<?php echo $pages; ?>/" style="<?php echo $GLOBALS["page"]>$pages ? "display: block;":""; ?>" class="next"></a>
		</div>
		</section>
		<script>
			$("#new-search select").each(function()
				{changenew(this);
				});
		</script>
<?php get_footer(); ?>