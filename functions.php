<?php
	add_theme_support('post-thumbnails');
	add_image_size('thumb-home', 121, 150, true);
	add_image_size('thumb-list', 284, 150, true);
	add_image_size('list-np', 133, 72, true);
	add_image_size('large-box', 244, 381, true);
	add_image_size('small-box', 226, 229, true);
	add_image_size('slider-a', 500, 235, true);
	add_image_size('small-box-a', 245, 245, true);
	add_image_size("column-banner", 240, 375, true);
	add_image_size("small-rectangle", 240, 140, true);
	add_image_size("three-one", 542, 250, true);
	add_image_size("three-two", 175, 250, true);
	add_image_size("three-three", 266, 250, true);
	add_image_size("post-list", 110, 155, true);
	add_image_size("big-banner", 744, 402, true);
	add_image_size("column-banner-index", 222, 402, true);
	add_image_size("small-box-index", 240, 211, true);
	add_image_size("small-banner", 240, 125, true);
	add_image_size("featured", 320, 140, true);
	add_image_size("home", 745, 355, true);

	function get_childrens($id, $sort)
		{$id=gettype($id)=="object" ? $id->ID : $id;
		$args = array(
			'sort_order' => 'ASC',
			'sort_column' => $sort,
			'child_of' => $id,
			'parent' => $id,
			'post_type' => 'page',
			'post_status' => 'publish'
			);
		
		$pages=get_pages($args);
		
		return count($pages)==0 ? false : $pages;
		}
	
	function construct_menu($id, $ok, $sort="menu_order", $class="")
		{if($sort=="")
			$sort="menu_order";
		
		$id=gettype($id)=="object" ? $id->ID : $id;
		if($childrens=get_childrens($id, $sort))
			{
?>
				<ul class="ul-menu<?php echo " " . $class; ?>">
<?php
			foreach($childrens as $v)
				{if(@in_array(get_post_meta($v->ID, '_wp_page_template', true), $ok) or $ok=="ALL")
					{
?>
					<li id="postID-<?php echo $v->ID; ?>">
						<a href="<?php echo get_permalink($v); ?>"><?php _e($v->post_title); ?></a>
<?php
						if($x=get_childrens($v->ID, $sort))
							{
?>
						<span class="plus-minus plus" onclick="plusMinus(this)" data-id="postID-<?php echo $v->ID; ?>"></span>
<?php
							construct_menu($v->ID, $ok, $sort);
							}
?>
					</li>
<?php
					}
				}
			}
?>
				</ul>
<?php
		}
	
	function get_post_wp($category, $quantity=-1, $offset=0)
		{$limit=$quantity>0 ? " limit " . $quantity*$offset . "," . $quantity : "";
		
		if(gettype($category)=="string")
			$tax="wp_term_relationships.term_taxonomy_id=" . get_cat_id($category);
		else
			{$tax="(";
			foreach($category as $i=>$v)
				{if($i!=0)
					$tax.=" or ";
				$tax.="wp_term_relationships.term_taxonomy_id=" . get_cat_id($v);
				}
			$tax.=")";
			}
		
		global $wpdb;
		return $wpdb->get_results("SELECT * FROM wp_term_relationships join wp_posts ON wp_posts.ID = wp_term_relationships.object_id where " . $tax . " and wp_posts.post_type like 'post' and wp_posts.post_status like 'publish' and wp_posts.post_title LIKE '%<!--:" . qtrans_getLanguage() . "-->%' group by wp_posts.ID order by wp_posts.post_date desc" . $limit);
		}

	function get_title($post_obj, $lang="", $limit=0)
		{$post_obj=gettype($post_obj)=="object" ? $post_obj : get_post($post_obj);
		if(gettype($lang)=="integer")
			{$limit=$lang;
			$lang="";
			}
		
		if($lang=="")
			$lang=qtrans_getLanguage();
		
		$blang=$GLOBALS["q_config"]["language"];
		$GLOBALS["q_config"]["language"]=$lang;
		
		$title=__($post_obj->post_title);
		$length=strlen($title);
		
		if($limit!=0)
			$title=substr($title, 0, $limit);
		
		while(@$title[strlen($title)-1]!=" " and $limit!=0 and $title!=strip_tags(__($post_obj->post_title)))
			$title.=substr(strip_tags(__($post_obj->post_title)), strlen($title), 1);
		
		if(@$title[strlen($title)-1]==" ")
			$title=substr($title, 0, strlen($title)-1);
		
		$title.=$limit===0 ? "" : $limit<$length ? "...":"";
		
		$GLOBALS["q_config"]["language"]=$blang;
		return $title;
		}

	function get_content($post_obj, $lang="", $limit=0)
		{$post_obj=gettype($post_obj)=="object" ? $post_obj : get_post($post_obj);
		if(gettype($lang)=="integer")
			{$limit=$lang;
			$lang="";
			}
			
		if($lang=="")
			$lang=qtrans_getLanguage();
		
		$blang=$GLOBALS["q_config"]["language"];
		$GLOBALS["q_config"]["language"]=$lang;
		
		$body=strip_shortcodes(__($post_obj->post_content));
		
		if($limit!=0)
			{$body=strip_tags($body);
			$length=strlen($body);
			$body=substr($body, 0, $limit);
			}
		
		while(@$body[strlen($body)-1]!=" " and $limit!=0 and $body!=strip_tags(__($post_obj->post_content)))
			$body.=substr(strip_tags(__($post_obj->post_content)), strlen($body), 1);
		
		if(@$body[strlen($body)-1]==" ")
			$body=substr($body, 0, strlen($body)-1);
		
		$body.=$limit===0 ? "" : $limit<$length ? "...":"";
		
		$GLOBALS["q_config"]["language"]=$blang;
		return $body;
		}

	function get_date($obj)
		{$return=Array();
		if(gettype($obj)=="integer")
			$obj=get_post($obj);
		
		$fields=get_field_objects($obj->ID);
		if(@$fields["date"]["value"]!="")
			$date=$fields["date"]["value"];
		else
			$date=$obj->post_date;
		
		$return[]=substr($date,0,4);
				
		switch(substr($date,5,2))
			{case "01":
				$month=array("Ene", "Jan", "Jan");
				break;
			case "02":
				$month=array("Feb", "Feb", "Fev");
				break;
			case "03":
				$month=array("Mar", "Mar", "Mar");
				break;
			case "04":
				$month=array("Abr", "Apr", "Abr");
				break;
			case "05":
				$month=array("May", "May", "Mai");
				break;
			case "06":
				$month=array("Jun", "Jun", "Jun");
				break;
			case "07":
				$month=array("Jul", "Jul", "Jul");
				break;
			case "08":
				$month=array("Ago", "Aug", "Ago");
				break;
			case "09":
				$month=array("Sep", "Sep", "Set");
				break;
			case "10":
				$month=array("Oct", "Oct", "Out");
				break;
			case "11":
				$month=array("Nov", "Nov", "Nov");
				break;
			case "12":
				$month=array("Dic", "Dec", "Dez");
				break;
			}
		
		switch(qtrans_getLanguage())
			{case "es":
				$n=0;
				break;
			case "en":
				$n=1;
				break;
			case "pt":
				$n=2;
				break;
			}
		
		$return[]=$month[$n];
		$return[]=substr($date, 8, 2);
		
		return $return;
		}

	function changeTemplateOnSave($id)
		{$post=get_post($id);
		if(get_post_meta($id, '_wp_page_template', true)=="default")
			{if(strpos($post->post_name, "-autosave-")===FALSE)
				{$parent_tmp = get_post_meta($post->post_parent, '_wp_page_template', true);
			
				if($parent_tmp=="b.php")
					update_post_meta($post->ID, '_wp_page_template', $parent_tmp);
				
				if($parent_tmp=="a.php" or $parent_tmp=="a-child.php")
					update_post_meta($post->ID, '_wp_page_template', "a-child.php");
				}
			}
		}
	add_action('save_post','changeTemplateOnSave');
	
	function get_featured($obj, $size, $lang="")
		{if(gettype($obj)=="integer")
			$obj=get_post($obj);
		
		if($obj->post_mime_type!="")
			{$img=wp_get_attachment_image_src($obj->ID, $size);
			return $img[0];
			}
		
		if($lang=="")
			$lang=qtrans_getLanguage();
		
		$blang=$GLOBALS["q_config"]["language"];
		$GLOBALS["q_config"]["language"]=$lang;
		
		if($lang=="en")
			{$img=wp_get_attachment_image_src(get_post_thumbnail_id($obj->ID), $size);
			$GLOBALS["q_config"]["language"]=$blang;
			return $img[0];
			}
			
		if($lang=="es")
			{$GLOBALS["q_config"]["language"]=$blang;
			return kd_mfi_get_featured_image_url('image-spanish', 'page', $size, $obj->ID);
			}
		
		if($lang=="pt")
			{$GLOBALS["q_config"]["language"]=$blang;
			return kd_mfi_get_featured_image_url('image-portuguese', 'page', $size, $obj->ID);
			}
		}
	
	if(!function_exists( 'amalgama_setup_block_pages' )):
	function amalgama_setup_block_pages()
		{global $block_public_pages;
		
		if(!is_user_logged_in())
			wp_die('Sorry, you must first <a href="' . site_url(). '/wp-login.php?redirect_to=' . $_SERVER["SCRIPT_URI"] . '">log in</a> to view this page.');
		
		}
		add_action('template_redirect', 'amalgama_setup_block_pages');
	endif;
	
	$args1 = array(
		'id' => 'image-spanish',
		'post_type' => 'page',
		'labels' => array(
			'name' => 'Featured Image Spanish (Español)',
			'set' => 'Set Spanish (Español)',
			'remove' => 'Remove  Spanish (Español)',
			'use' => 'Use as Spanish (Español)'
			)
		);

	$args2 = array(
		'id' => 'image-portuguese',
		'post_type' => 'page',
		'labels' => array(
			'name' => 'Featured Portuguese (Português)',
			'set' => 'Set Portuguese (Português)',
			'remove' => 'Remove Portuguese (Português)',
			'use' => 'Use as Portuguese (Português)'
			)
		);

	$args3 = array(
		'id' => 'image-spanish',
		'post_type' => 'post',
		'labels' => array(
			'name' => 'Featured Image Spanish (Español)',
			'set' => 'Set Spanish (Español)',
			'remove' => 'Remove  Spanish (Español)',
			'use' => 'Use as Spanish (Español)'
			)
		);

	$args4 = array(
		'id' => 'image-portuguese',
		'post_type' => 'post',
		'labels' => array(
			'name' => 'Featured Portuguese (Português)',
			'set' => 'Set Portuguese (Português)',
			'remove' => 'Remove Portuguese (Português)',
			'use' => 'Use as Portuguese (Português)'
			)
		);

	new kdMultipleFeaturedImages($args1);
	new kdMultipleFeaturedImages($args2);
	new kdMultipleFeaturedImages($args3);
	new kdMultipleFeaturedImages($args4);
	
	function link_to_follow($obj, $build=true)
		{if(gettype($obj)=="integer")
			$obj=get_post($obj);
		else if(gettype($obj)=="string")
			$obj=get_post((int) $obj);
		
		$fields=get_field_objects($obj->ID);
		
		if($obj->post_mime_type!="")
			$return=get_post_meta($obj->ID, '_gallery_link_url', true);
		else
			{if(isset($fields["redirect_to"]["value"]))
				if($fields["redirect_to"]["value"]!="")
					$return=$fields["redirect_to"]["value"];
			
			if(!isset($return))
				$return=get_permalink($obj->ID);
			}
		
		if($build)
			{$return='href="' . $return . '"';
			if($obj->post_mime_type!="")
				{$target=get_post_meta($obj->ID, '_gallery_link_target', true);
				if($target=="")
					$target="default";
				}
			else
				{if(isset($fields["target"]["value"]))
					$target=$fields["target"]["value"];
				else
					$target="default";
				}
			
			if($target!="default")
				$return.=' target="' . $target . '"';
			}
		
		return $return;
		}
	
	function myplugin_add_meta_box()
		{$screens = array('post', 'page', "attachment");
		foreach($screens as $screen)
			{$args = array(
				'sort_order' => 'ASC',
				'sort_column' => 'menu_order',
				'child_of' => 3272,
				'parent' => 3272,
				'post_type' => 'page',
				'post_status' => 'publish'
				);
			
			$pages = get_pages($args);
			foreach($pages as $v)
				{if(get_post_meta($v->ID, '_wp_page_template', true)=="a.php")
					add_meta_box($v->post_title, $v->post_title, 'myplugin_meta_box_callback', $screen);
				}
			}
		
		foreach($screens as $screen)
			{if($screen=="attachment")
				continue;
			add_meta_box("Featured", "Featured", 'template_callback', $screen);
			}
		}
	add_action('add_meta_boxes', 'myplugin_add_meta_box');
	
	function myplugin_meta_box_callback($post, $v)
		{wp_nonce_field($v["id"], $v["id"] . '_nonce');
		$value = get_post_meta($post->ID, "size_" . $v["id"], true);
		
		echo 'Size in home of A group <select id="size_' . $v["id"]. '" name="size_' . $v["id"]. '">';
		
		$select=esc_attr($value)=="None" ? " selected=\"selected\"":"";
		echo '<option value="None"' . $select . '>None</option>';
		
		$select=esc_attr($value)=="Small Box" ? " selected=\"selected\"":"";
		echo '<option value="Small Box"' . $select . '>Small Box</option>';
		
		$select=esc_attr($value)=="Small Box With Text" ? " selected=\"selected\"":"";
		echo '<option value="Small Box With Text"' . $select . '>Small Box With Text</option>';
		
		$select=esc_attr($value)=="Small rectangle box" ? " selected=\"selected\"":"";
		echo '<option value="Small rectangle box"' . $select . '>Small rectangle box</option>';
		
		$select=esc_attr($value)=="Column Banner" ? " selected=\"selected\"":"";
		echo '<option value="Column Banner"' . $select . '>Column Banner</option>';
		
		echo '</select>';
		}
	
	function template_callback($post, $v)
		{wp_nonce_field($v["id"], $v["id"] . '_nonce');
		$value = get_post_meta($post->ID, "size_" . $v["id"], true);
		
		echo 'Show in the next Featured Pages:';
		$pages=get_posts(array(
			'post_type' => 'page',
			'sort_column' => 'post_date',
			'orderby' => 'post_date',
			'order' => 'ASC',
			'posts_per_page' => -1,
			'meta_query' => array(
				'relation' => 'OR',
				array(
					'key' => '_wp_page_template',
					'value' => "featured-pages.php",
					'compare' => '='
				))));
		$featured=get_post_meta($post->ID, "featured", true);
		if($featured!="")
			$featured=implode(" ", $featured);
		
		foreach($pages as $p)
			{$checked="";
			if(strpos($featured, $p->post_name)!==false)
				$checked='checked="checked"';
			echo '<br /><input type="checkbox" id="featured_' . $p->post_name. '" name="featured_' . $p->post_name . '" value="' . $p->post_name . '" ' . $checked . ' /> <label for="featured_' .$p->post_name . '">' . __($p->post_title) .'</label>';
			}
		}
	
	function myplugin_save_meta_box_data($post_id)
		{if(!isset($_POST["autosave"]))
			{$args = array(
				'sort_order' => 'ASC',
				'sort_column' => 'menu_order',
				'child_of' => 3272,
				'parent' => 3272,
				'post_type' => 'page',
				'post_status' => 'publish'
				);
			
			$pages = get_pages($args);
			$keys=array();
			foreach($pages as $v)
				if(get_post_meta($v->ID, '_wp_page_template', true)=="a.php")
					$keys[]=$v->post_title;
			
			$data=array();
			foreach($keys as $v)
				if(isset($_POST['size_' . $v]))
					$data[]=sanitize_text_field($_POST['size_' . $v]);
			
			if(count($data))
				foreach($keys as $i=>$v)
					update_post_meta($post_id, "size_" . $v, $data[$i]);
			}
		}
	add_action('save_post', 'myplugin_save_meta_box_data');
	add_action('edit_attachment', 'myplugin_save_meta_box_data');
	
	function featured_save($id)
		{if(!isset($_POST["autosave"]))
			{$pages=get_posts(array(
				'post_type' => 'page',
				'sort_column' => 'post_date',
				'orderby' => 'post_date',
				'order' => 'ASC',
				'posts_per_page' => -1,
				'meta_query' => array(
					'relation' => 'OR',
					array(
						'key' => '_wp_page_template',
						'value' => "featured-pages.php",
						'compare' => '='
					))));
				
			$data=array();
			foreach($pages as $v)
				{if(isset($_POST['featured_' . $v->post_name]))
					$data[]=sanitize_text_field($_POST['featured_' . $v->post_name]);
				}
			
			if(count($data))
				{if(isset($_POST["featured"]))
					update_post_meta($id, "featured", $data);
				else
					add_post_meta($id, "featured", $data);
				}
			}
		}
	add_action('save_post', 'featured_save');
	
	function dump($obj)
		{
?>
	<textarea style="display: none;"><?php var_dump($obj); ?></textarea>
<?php
		}
	
	function get_posts_date($category, $quantity=0, $offset=0)
		{global $post;
		if(isset($_POST["montht"]))
			$_SESSION["montht"][$post->post_tilte]=$_POST["montht"];
		
		if(isset($_POST["yearf"]))
			$_SESSION["yearf"][$post->post_tilte]=$_POST["yearf"];
		
		if(isset($_POST["monthf"]))
			$_SESSION["monthf"][$post->post_tilte]=$_POST["monthf"];
		
		if(isset($_POST["yeart"]))
			$_SESSION["yeart"][$post->post_tilte]=$_POST["yeart"];
		
		if(isset($_POST["search"]))
			$_SESSION["search"][$post->post_tilte]=$_POST["search"];
		
		switch($_SESSION["montht"][$post->post_tilte])
			{case '01':
			case '03':
			case '05':
			case '07':
			case '08':
			case '10':
			case '12':
				$d=31;
				break;
			case '02':
				$d=$_SESSION["montht"][$post->post_tilte]%4==0 ? 29:28;
				break;
			case '04':
			case '06':
			case '09':
			case '11':
				$d=30;
				break;
			}
		
		$date=date_create($_SESSION["yearf"][$post->post_tilte] . '-' . $_SESSION["monthf"][$post->post_tilte] . '-01');
		$datef=date_format($date, 'Y-m-d');
		
		$date = date_create($_SESSION["yeart"][$post->post_tilte] . '-' . $_SESSION["montht"][$post->post_tilte] . '-' . $d);
		$datet=date_format($date, 'Y-m-d');
		
		$limit=$quantity>0 ? " limit " . $quantity*$offset . "," . $quantity : "";
		
		if(gettype($category)=="integer")
			$tax="wp_term_relationships.term_taxonomy_id=" . $category;
		else
			{$tax="(";
			foreach($category as $i=>$v)
				{if($i!=0)
					$tax.=" or ";
				$tax.="wp_term_relationships.term_taxonomy_id=" . $v;
				}
			$tax.=")";
			}
		
		global $wpdb;
		return $wpdb->get_results("SELECT * FROM wp_term_relationships join wp_posts ON wp_posts.ID = wp_term_relationships.object_id where " . $tax . " and wp_posts.post_type like 'post' and wp_posts.post_status like 'publish' and wp_posts.post_date >= '" . $datef . "' AND wp_posts.post_date <= '" . $datet . "' and wp_posts.post_content LIKE '%<!--:" . qtrans_getLanguage() . "-->%' group by wp_term_relationships.object_id" . $limit);
		}

	function build_b($arg, $n=1)
		{global $post;
?>
			<ul data-id="<?php echo $arg->ID; ?>" data-level="<?php echo $n; ?>">
<?php
		$args = array(
			'sort_order' => 'ASC',
			'sort_column' => 'menu_order',
			'child_of' => $arg->ID,
			'parent' => $arg->ID,
			'post_type' => 'page',
			'post_status' => 'publish');
		
		$pages=get_pages($args);
		foreach($pages as $v)
			{$parents=get_post_ancestors($v);
			$li="";
			if(in_array($v->ID, $parents) or $v->ID==$post->ID)
				$li="li-active";
?>
				<li class="<?php echo $li; ?>" data-deploy="<?php echo $v->ID; ?>"><a target="_self" href="<?php echo get_permalink($v->ID); ?>" class="<?php //echo $v->ID==$post->ID ? "active" : ""; ?>"><?php _e($v->post_title); ?></a></li>
<?php		} ?>
			</ul>
<?php
		$n++;
		foreach($pages as $v)
			{$args = array(
				'sort_order' => 'ASC',
				'sort_column' => 'menu_order',
				'child_of' => $v->ID,
				'parent' => $v->ID,
				'post_type' => 'page',
				'post_status' => 'publish');
			
			if(count(get_pages($args)))
				build_b($v, $n);
			}
		}
?>